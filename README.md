![alt text](https://github.com/matt360/gef_box2d_collisions_app/blob/master/media/png/title_screen.png)

![alt text](https://github.com/matt360/gef_box2d_collisions_app/blob/master/media/png/instructions_1.png)

Keyboard controls:

Use `arrows` to navigate the menus.

`escape` or `backspace` - toggle the pause menu.

`return` - confrim the menu choice.

`space` - jump.

`left ctrl` or `right shift` - change the player's colour.

`m` - toggle the fps display.

`c` - toggle the camera.

`WSAD` - move the camera.

`Q` and `E` - change the camera's Y position.

`4NUM` and `6NUM` - change the camera's yaw.

`8NUM` and `2NUM` - change the camera's pitch.

